# ジャッジシステム

このプロジェクトでは，dockerを使用した簡易的なオンラインジャッジシステムを作成しています．

<br />
<br />

## 現在の対応言語（2022年10月28日）
- Python

<br />
<br />


# デモ
<img src="https://i.gyazo.com/8d875f4c82b1d2bad9c54815a663c186.gif" alt="drawing" width="480"/>


<br />
<br />

# 使用方法
```
make setup
```
ユーザーから提出されたプログラムを実行するコンテナのイメージを作成します.

"judge-image"というイメージが追加されます．

<br />

```
make run app
```
アプリケーションが起動します．


http://localhost:3000 にアクセスできます．

<br />

```
make clean
```
上記のコマンドで作成したdockerのコンテナとイメージを削除します．

