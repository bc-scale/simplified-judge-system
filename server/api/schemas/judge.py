from typing import Optional

from pydantic import BaseModel, Field


class JudgeRequest(BaseModel):
    source_code: Optional[str] = Field(
        None, description="ソースコード", example='print("Hello", input())'
    )
    stdin: Optional[str] = Field(None, description="標準入力", example="Python")

    class Config:
        orm_mode = True


class JudgeResponse(BaseModel):
    stdout: Optional[str] = Field(None, description="標準出力")
    stderr: Optional[str] = Field(None, description="標準出力エラー")
    err_type: Optional[str] = Field(None, description="")
    execution_time: Optional[float] = Field(None, description="実行時間")

    class Config:
        orm_mode = True
