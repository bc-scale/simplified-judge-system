import subprocess
from datetime import datetime

import api.schemas.judge as judge_schema
from fastapi import APIRouter

router = APIRouter(prefix="/judge", tags=["judge"])


@router.post("/", response_model=judge_schema.JudgeResponse)
async def judge(body: judge_schema.JudgeRequest):
    source_code = body.source_code
    stdin = body.stdin

    response_data = {
        "stdout": "",
        "stderr": "",
        "error_type": None,
        "execution_time": 0.0,
    }

    image_name = "judge-image"
    exec_cmd = "python3 Main.py"
    docker_cmd = f'docker create -i \
--net none \
--cpuset-cpus 0 \
--memory 512m --memory-swap 512m \
--ulimit nproc=10:10 \
--ulimit fsize=1000000 \
-w /workspace \
{image_name} \
/usr/bin/time -q -f "%e" -o /time.txt \
timeout 2 \
su nobody -s /bin/bash -c "{exec_cmd}"'

    container_id = ""
    try:
        result = subprocess.run(
            docker_cmd,
            shell=True,
            text=True,
            check=True,
            timeout=2,
            capture_output=True,
        )
        container_id = result.stdout[:12]
    except subprocess.CalledProcessError as e:
        print("process error: ", e)
    except subprocess.TimeoutExpired as e:
        print("timeout expired", e)

    # ファイル作成
    workdir = "/tmp/workspace"
    cmd = f"rm -rf {workdir} && mkdir {workdir} && chmod 777 {workdir}"
    proc = subprocess.Popen(
        cmd,
        shell=True,
        text=True,
    )
    try:
        proc.communicate(timeout=2)
    except subprocess.CalledProcessError as e:
        print("process error: ", e)
    except subprocess.TimeoutExpired as e:
        proc.kill()
        print("timeout expired", e)

    with open(f'{workdir}/{"Main.py"}', "w") as f:
        f.write(source_code)

    # コンテナにコピー
    try:
        docker_cmd = f"docker cp {workdir} {container_id}:/"
        subprocess.run(
            docker_cmd,
            shell=True,
            text=True,
            check=True,
            timeout=2,
        )

    except subprocess.CalledProcessError as e:
        print("process error: ", e)
    except subprocess.TimeoutExpired as e:
        print("timeout expired", e)

    # コンテナ起動
    docker_cmd = f"docker start -i {container_id}"
    proc = subprocess.Popen(
        docker_cmd,
        shell=True,
        text=True,
        stdin=subprocess.PIPE,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
    )
    try:
        start_time = datetime.now().timestamp() * 1000
        out, err = proc.communicate(input=stdin, timeout=2)
        end_time = datetime.now().timestamp() * 1000

        response_data["stdout"] = out
        response_data["stderr"] = err
        response_data["execution_time"] = end_time - start_time

        if response_data["stderr"]:
            response_data["err_type"] = "RE"

    except subprocess.CalledProcessError as e:
        print("process error: ", e)
        response_data["error_type"] = "MLE"
    except subprocess.TimeoutExpired as e:
        proc.kill()
        response_data["error_type"] = "TLE"
        response_data["execution_time"] = 2.0
        print("timeout expired", e)

    # コンテナ削除
    docker_cmd = f"docker rm -f {container_id}"
    proc = subprocess.Popen(
        docker_cmd,
        shell=True,
        text=True,
    )
    try:
        proc.communicate()
    except subprocess.CalledProcessError as e:
        print("process error: ", e)
    except subprocess.TimeoutExpired as e:
        print("timeout expired", e)

    return response_data
