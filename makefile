setup:
		docker build -t judge-image docker/
run app:
		docker-compose build && docker-compose up -d
clean:
		docker-compose down --rmi all && docker rmi judge-image
test:
		docker-compose exec server poetry run pytest